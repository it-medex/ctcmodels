﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class DrawLines : MonoBehaviour {

    public Material materiale;
    
    [Header("Path for the file containing the centerline points")]
    [SerializeField]
    public string filepath;

    private List<Vector3> centerline;
    private List<Vector3> centerlineOutOfOrder;
    private Transform splineRootTransform;

    void Start() {

        ////Obtain Centerline Points
        ControlPointsParser parser = new ControlPointsParser(filepath);
        centerlineOutOfOrder = parser.getSplinePoints();

        ///Order Centerline Points
        NearestNeighborList nneighbors = new NearestNeighborList(centerlineOutOfOrder);
        centerline = nneighbors.getCenterline();

        //Apply 3D Model's Transformation to the Centerline
        splineRootTransform = GetComponent<Transform>();


        for (int j = 1; j < centerline.Count; j++)
        {
            GameObject myLine = new GameObject();
            myLine.transform.position = splineRootTransform.position + centerline[j - 1];
            myLine.transform.rotation = splineRootTransform.rotation;
            myLine.AddComponent<LineRenderer>();
            LineRenderer lr = myLine.GetComponent<LineRenderer>();
            lr.material = materiale;

            lr.startWidth = 0.1f;
            lr.endWidth = 0.1f;

            Vector3 startPt = new Vector3(splineRootTransform.position.x + centerline[j - 1].x, splineRootTransform.position.y + centerline[j - 1].y, splineRootTransform.position.z + centerline[j - 1].z);
            Vector3 endPt = new Vector3(splineRootTransform.position.x + centerline[j].x, splineRootTransform.position.y + centerline[j].y, splineRootTransform.position.z + centerline[j].z);

            startPt = splineRootTransform.rotation * startPt;
            endPt = splineRootTransform.rotation * endPt;

            lr.SetPosition(0, startPt);
            lr.SetPosition(1, endPt);

        }

    }

}

	