﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NearestNeighborList : MonoBehaviour {

    private List<Vector3> splineControlPoints;

    public NearestNeighborList(List<Vector3> pts)
    {
        orderCenterlinePts(pts);
    }

    public List<Vector3> getCenterline()
    {
        return splineControlPoints;
    }

    //Order list of points according to the minimum pairwise distance
    public void orderCenterlinePts(List<Vector3> pts)
    {
        int N = pts.Count;
        float[,] dist = new float[N,N];

        int[] i = new int[N];

        //Centerline's Starting Point
        //Find smallest element in z column and corresponding index

        float minZ = float.MaxValue;
        int startPtIndex = 0;

        for (int j=0; j<N; j++)
        {
            float z = pts[j].z;

            if (z < minZ)
            {
                minZ = z;
                startPtIndex = j;
            }
        }

        //Save the index of the first point and add it to the list
        i[0] = startPtIndex;

        splineControlPoints = new List<Vector3>();
        splineControlPoints.Add(pts[startPtIndex]);


        //Calculate pairwise distances between all points
        for (int n=0; n<N; n++)
        {
            for (int nn=0; nn<N; nn++)
            {
                dist[n, nn] = Vector3.Distance(pts[n], pts[nn]);
            }
        }

       
       //Order list of points by index

       for (int k=1; k<N; k++)
        {
            float minDist = float.MaxValue;

            for(int j=0;j<N;j++) dist[j, i[k - 1]] = float.PositiveInfinity;

            for (int p = 0; p < N; p++)
            {
                float d = dist[i[k - 1],p];

                if (d < minDist)
                {
                    minDist = d;
                    i[k] = p;
                }              
                                                  
            }
            //Adjust the sense of vector X to match Unity's
            splineControlPoints.Add(new Vector3(-pts[i[k]].x, pts[i[k]].y, pts[i[k]].z));
        }

    }
    

}
