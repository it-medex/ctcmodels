﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlPointsParser : MonoBehaviour
{

    private List<Vector3> splinePoints;

    public ControlPointsParser(string file1)
    {
        ParsePointListFile(file1);
    }

    public List<Vector3> getSplinePoints()
    {
        return splinePoints;
    }
    

    //Function responsible for extracting 3D points coordinates
    public void ParsePointListFile(string fileFolder)
    {
        System.IO.StreamReader file =
            new System.IO.StreamReader(fileFolder);

        string line;

        splinePoints = new List<Vector3>();

        while ((line = file.ReadLine()) != null)
        {
            Vector3 p = new Vector3();

            var pt = line.Split(" "[0]);

            // To address only the lines corresponding to points positions
            if (pt[0].Equals("v"))
            {
                float x = float.Parse(pt[1]);
                float y = float.Parse(pt[2]);
                float z = float.Parse(pt[3]);

                p = new Vector3(x, y, z);

                splinePoints.Add(p);
            }
        }
    }

}
